;(function( $ ){
  'use strict'

  var pluginName = "weather",
  defaults = {
    position: {
      city: null,
      lat: null,
      lon: null,
    },
    key: '2de143494c0b295cca9337e1e96b00e0',
    apiURL: 'http://api.openweathermap.org/data/2.5/weather?',
    detectLoc: true, 
  };

  var autocompleteOptions = {
    serviceUrl: "http://gd.geobytes.com/AutoCompleteCity?callback=?",
    dataType: 'jsonp',
    paramName: 'q',
    transformResult: responce => 0||{suggestions: responce},
    minChars: 3,
    deferRequestBy: 500,
  };

  function Plugin ( el, options ) {
    this.el = $(el);
    this._name = pluginName + this.el.index();
    this.settings = $.extend( {}, defaults, options, this._getState(this._name) );
    this._defaults = defaults;
    this.init();
  };

  $.fn[ pluginName ] = function ( options ) {
    this.each(function() {
      if ( !$.data( this, "plugin_" + pluginName ) ) {
        $.data( this, "plugin_" + pluginName, new Plugin( this, options ) );
      }
    });

    return this;
  };

  $.extend(Plugin.prototype, {

    init: function () {
      this._execute( this._run() );
    },

    _run: function* () {

      var currLocation = yield this._getLocation();            

      while(true) {

        if(!currLocation) {
          this._renderWitget();
          currLocation = yield this._setCity();
        } 
        this._renderSpinner();
        this._setLocation(currLocation);

        var url = this._getUrl();
        var wheatherFetch = yield fetch(url);
        var wheatherInfo = yield wheatherFetch.json();
        var wheatherData = this._parseData(wheatherInfo);

        this._renderWitget(wheatherData);
        if (wheatherData.cod) {
          currLocation = null;
          return;
        }
        this._saveState();
        this.el.find('.locat__remember_inp')
               .click(this._changeDetectLoc.bind(this));
        currLocation = yield this._changeCity(this.el);
      }
    },

    _changeDetectLoc: function(e) {
      this.settings.detectLoc = e.target.checked;
      this._saveState();
    },

    _saveState: function() {
        var s = this.settings;
        var newState = s.detectLoc ? {position: s.position} : {detectLoc: false};
        localStorage.setItem(this._name, JSON.stringify(newState));
    },
    _getState: function (key) {
      return JSON.parse(localStorage.getItem(key));
    },

    _setLocation: function(data) {
      var position = this.settings.position;
        if(data.coords) {
          position.lat = data.coords.latitude;
          position.lon = data.coords.longitude;
        } else {
          position.city = data.value;
        }
    },

    _getUrl: function(coord) {
      var s = this.settings;
      var loc = s.position.city ? 
              `&q=${s.position.city}` : 
              `&lat=${s.position.lat}&lon=${s.position.lon}`;
      return `${s.apiURL + loc}&appid=${s.key}`;
    },

    _getLocation: function() {
      return new Promise((resolve, reject) => {
        var p = this.settings.position;
        if(p.city){
          resolve( { value: p.city } );
        }  
        else if (p.lat && p.lon) {
          resolve( { coords: { latitude: p.lat, longitude: p.lon } });
        }                 
        else if(navigator.geolocation) {
          p.geo = true;
          navigator.geolocation.getCurrentPosition(resolve, reject, { timeout: 5000 });
        } else
          resolve(null);
      });
    },

    _parseData: function(data){
      if (data.cod == 404){
        return null;
      }
      var parsedData = {};
      var KELVIN = 273.15; 

      parsedData.temp = ~~(data.main.temp - KELVIN) + '°C';
      parsedData.minTemp = ~~(data.main.temp_min - KELVIN) + '°C';
      parsedData.maxTemp = ~~(data.main.temp_max - KELVIN) + '°C';
      parsedData.icon = data.weather[0].icon;
      parsedData.name = data.name;
      parsedData.country = data.sys.country;
      return parsedData;
    },

    _setCity: function () {
      var inp = this.el.find(".get-town");
      return  new Promise(resolve => {
        inp.keyup(function (e) {
          if (e.keyCode == 13 && e.target.value)
            resolve( {value: e.target.value} );            
        });

        if ($.fn.autocomplete){
          autocompleteOptions.onSelect = resolve;
          inp.autocomplete(autocompleteOptions);
        }

      });
    },

    _changeCity: (el) => new Promise(resolve => {
      el.find(".city").one('click', () => resolve(null));
    }),

    _renderSpinner: function () {
      var template = `<div class="load-container load5">
                        <div class="loader">Loading...</div>
                      </div>`;
      this.el.html(template);
    },

    _renderWitget: function(data) {
      var template;
      if (data) {
        var detectLoc = this.settings.detectLoc;
        template = `${ !detectLoc ? "<span class='navigate-icon'>&#10148;</span>" : ""}
        <h2 class="city" title='click to change city'>${data.name},${data.country}</h2>
        <img src="http://openweathermap.org/img/w/${data.icon}.png" alt="" class="pict">
        <span class="temp-text temp">${data.temp}</span>
        <div class="widget-text">
        <span class="temp-text temp-min">от ${data.minTemp}</span>
        <span class="temp-text temp-max">до ${data.maxTemp}</span>
        </div>
        <div class="locat__remember">
        <input class="locat__remember_inp" type="checkbox" ${detectLoc ? "checked" : ""}>
        <span class="locat__remember_text">remember my location</span>
        </div>`
      } else {
        template = `<div class="inp-city-wrap">
        <input type='text' class='get-town' placeholder='Укажите город' autofocus>
        </div>`
        if (data && data.cod)
          template += "<span>" + data.message + "</span>"
      }
      this.el.html(template);
    },

    _execute: function (generator, yieldValue) {
      var next = generator.next(yieldValue);
      if (!next.done) {
        next.value.then(
          result => this._execute(generator, result),
          err => this._execute(generator, null)
          );
      }
    },

  });


})( jQuery );